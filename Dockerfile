FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/default.conf
COPY htpasswd /etc/nginx/conf.d/htpasswd
COPY . /usr/share/nginx/html
EXPOSE 80
